import React from 'react'

//create a context Object

//A context object as the same states is a data type of an object that can be used to store information that can be shared to other components within the app
//the context object is a different approach to passong information between components and allows easier access by avoiding the use of prop drilling

const UserContext = React.createContext()

//the "Provider" property of createContext allows other components to consume/use the context object and supply the necessary information
export const UserProvider = UserContext.Provider

export default UserContext