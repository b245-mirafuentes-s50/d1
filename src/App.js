
import './App.css';
//importing AppNavBar function from the AppNavBar.js
import AppNavBar from './components/AppNavBar.js'
import Home from './pages/Home.js'
import Course from './pages/Course.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Logout from './pages/Logout.js'
import PageNotFound from './pages/PageNotFound.js'
import CourseView from './components/CourseView.js'

import {useState, useEffect} from 'react'

//import the UserProvider
import {UserProvider} from './UserContext.js'

//import modules from react-router-dom for the routing 
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'

function App() {
  const [user, setUser] = useState(null)

  useEffect(() => {
    console.log(user)
  }, [user])

  const unSetUser = () => {
    localStorage.clear()
  }
  useEffect(()=> {
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}` 
      }
    })
    .then(result => result.json())
    .then(data => {
      console.log(data)
      if(localStorage.getItem('token') !== null){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser(null)
      }
      
    })
  }, [])

  //storing information in a context object is done by providing the information using the corresponding "Provider" and passing information thru the prop value
  //all information/data provided to the "Provider" component can be access later on from the context object properties

  return (
   <UserProvider value={{user, setUser, unSetUser}}>
    <Router>
      <AppNavBar/>
      <Routes>
          <Route path="/" element = {<Home/>}/>
          <Route path="/courses" element = {<Course/>}/>
          <Route path="/login" element = {<Login/>}/>
          <Route path="/register" element = {<Register/>}/>
          <Route path="/Logout" element = {<Logout/>}/>
          <Route path="*" element = {<PageNotFound/>}/>
          <Route path="/courseView/:courseId" element = {<CourseView/>}/>
      </Routes>
    </Router>
   </UserProvider>
  );
}

export default App;
