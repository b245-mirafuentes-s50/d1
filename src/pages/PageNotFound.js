import {Fragment} from 'react'

export default function PageNotFound (){
	return(
		<Fragment>
			<h1>Page Not Found</h1>
			<p>Go back to the <a href="/">homepage</a></p>
		</Fragment>
		)
}