import Card from 'react-bootstrap/Card';
import {Row, Col, Button} from 'react-bootstrap'
import {useState} from 'react'
import {useEffect, useContext} from 'react'
import UserContext from '../UserContext.js'
import {Link} from 'react-router-dom'

export default function Courses ({courseProp}) {

	const {_id, name, description, price} = courseProp
		// The "course" in the Courses component is called a "prop" which is a shorthand for property
		// The curly braces are used for prop to signify that we are providing information using expression

	//Use the state hook for this component to be able to store its state
	//States are used to keep track of information related to individual components
		// const [getter, setter] = useState(initialGetterValue)
	const [enrollees, setEnrollees] = useState(0)

	//initial value of enrollees state
		console.log(enrollees)

	//if you want to change/reassign the value of the state
		// setEnrollees(1)
		// console.log(enrollees)

	const [seats, setSeats] = useState(30)
	//add new state that will declare whether the button is disabled or not
	const [isDisabled, setIsDisabled] = useState(false)

	const {user} = useContext(UserContext)

	function enroll (){
		setEnrollees(enrollees+1)
			if(enrollees === 30){
				return setEnrollees(30)
			}
		setSeats(seats-1)
			if(seats === 0){
				return setSeats(0)
			}
	}

	// Define a 'useEffect' hook to have the "Courses" component do perform a centain task

	//This will run automatically
	//Syntax:
		//useEffect(sideEffect/function, [dependecies])

	//sideEffect/function - it will run on the first load and will reload depending on the dependency array

	useEffect(() =>{
		if(seats === 0){
			alert("congrats for making it to the cut")
			setIsDisabled(true)
		}
	}, [seats])

	return(
		<Row className="mt-4">
			{/*first card*/}
			<Col>
				<Card>
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Text>
				        	<Card.Subtitle>Description:</Card.Subtitle>
				        	<Card.Text>{description}</Card.Text>
				        	<Card.Subtitle>price:</Card.Subtitle>
				        	<Card.Text>Php {price}</Card.Text>
				        	<Card.Subtitle>Enrollees</Card.Subtitle>
				        	<Card.Text>{enrollees}</Card.Text>
				        	<Card.Subtitle>Available Seats:</Card.Subtitle>
				        	<Card.Text>{seats}</Card.Text>
				        </Card.Text>
				        {
				        	user ?
				        	<Button as = {Link} to = {`/courseView/${_id}`} disabled = {isDisabled} onClick={()=> enroll(_id)} >See More Details</Button>
				        	:
				        	<Button as = {Link} to = "/login">Login</Button>
				        }
				        
				      </Card.Body>
				</Card>
			</Col>
		</Row>
		)
}