import Card from 'react-bootstrap/Card';
import {Row, Col} from 'react-bootstrap'
export default function Highlights () {
	return(
			<Row>
				{/*first card*/}
				<Col className="col-md-4 col-10 mx-auto m-md-0 m-1">
					<Card className="cardHighlights">
					      <Card.Body>
					        <Card.Title>Learn From Home</Card.Title>
					        <Card.Text>
					          Some quick example text to build on the card title and make up the
					          bulk of the card's content.
					        </Card.Text>
					      </Card.Body>
					</Card>
				</Col>

				{/*Second Card*/}
				<Col className="col-md-4 col-10 mx-auto m-md-0 m-1">
					<Card className="cardHighlights">
					      <Card.Body>
					        <Card.Title>Study Now, Pay Later</Card.Title>
					        <Card.Text>
					          Some quick example text to build on the card title and make up the
					          bulk of the card's content.
					        </Card.Text>
					      </Card.Body>
					</Card>
				</Col>

				{/*third card*/}
				<Col className="col-md-4 col-10 mx-auto m-md-0 m-1">
					<Card className="cardHighlights">
					      <Card.Body>
					        <Card.Title>Be Part of our Community</Card.Title>
					        <Card.Text>
					          Some quick example text to build on the card title and make up the
					          bulk of the card's content.
					        </Card.Text>
					      </Card.Body>
					</Card>
				</Col>
			</Row>
		)
}